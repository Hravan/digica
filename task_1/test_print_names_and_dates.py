import pandas as pd

from print_names_and_dates import filter_dates, filter_feminine_names


class TestFilterDates:
    def test_df_one_date_to_remain(self):
        df = pd.Series(['2000-01-01'], name='data_urodzenia').astype('datetime64[ns]').to_frame()
        pd.testing.assert_frame_equal(filter_dates(df), df)

    def test_df_one_date_to_remove(self):
        df = pd.Series(['1999-12-31'], name='data_urodzenia').astype('datetime64[ns]').to_frame()
        expected_out = pd.DataFrame(columns=['data_urodzenia'])
        pd.testing.assert_frame_equal(filter_dates(df), expected_out, check_index_type=False, check_dtype=False)

    def test_df_two_dates_one_to_remain(self):
        df = pd.Series(['1999-12-31', '2000-01-01'], name='data_urodzenia').astype('datetime64[ns]').to_frame()
        expected_out = pd.Series(['2000-01-01'], name='data_urodzenia').astype('datetime64[ns]').to_frame()
        pd.testing.assert_frame_equal(filter_dates(df), expected_out, check_dtype=False, check_index_type=False)


class TestFilterFeminineNames:
    def test_df_one_name_to_remain(self):
        df = pd.DataFrame([('Ewa')], columns=['imie'])
        assert filter_feminine_names(df) == ['Ewa']

    def test_df_one_name_to_remove(self):
        df = pd.DataFrame([('Rafal')], columns=['imie'])
        assert filter_feminine_names(df) == []

    def test_df_two_names_one_to_remain(self):
        df = pd.DataFrame([('Ewa'), ('Rafal')], columns=['imie'])
        assert filter_feminine_names(df) == ['Ewa']

    def test_two_names_one_feminine_duplicated(self):
        df = pd.DataFrame([('Ewa'), ('Rafal'), ('Ewa')], columns=['imie'])
        assert filter_feminine_names(df) == ['Ewa']


