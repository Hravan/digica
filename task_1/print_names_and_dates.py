import argparse
from typing import List

import pandas as pd


def read_people_csv(csv_path: str) -> pd.DataFrame:
    """Read csv file from given path and return a pandas DataFrame with column data_urodzenia converted to
    a datetime Series."""
    people_df = pd.read_csv(csv_path)
    people_df['data_urodzenia'] = pd.to_datetime(people_df['data_urodzenia'], format='%d.%m.%Y')
    return people_df


def filter_dates(df: pd.DataFrame) -> pd.DataFrame:
    """Consume a data frame with a datetime column data_urodzenia and return a new DataFrame only with rows
    with data_urodzenia after 1999-12-31."""
    mask = df['data_urodzenia'] > '1999-12-31'
    df = df[mask]
    df = df.reset_index(drop=True)
    return df


def filter_feminine_names(df: pd.DataFrame) -> List[str]:
    """Consume a data frame with a column imie containing strings and return a list of unique strings form that column
    that end with an 'a'."""
    return df['imie'][df['imie'].str.endswith('a')].unique().tolist()


def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--csv_path', default='data/zadanie1.csv')
    args = arg_parser.parse_args()
    people_df = read_people_csv(args.csv_path)
    filtered_by_date_df = filter_dates(people_df)
    print(f'{len(filtered_by_date_df)} osób urodziło się po 31 grudnia 1999 roku.')
    print(f'Imiona żeńskie: {", ".join(filter_feminine_names(people_df))}')


if __name__ == '__main__':
    main()
