import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sqlite3


RED_MIN = np.array([0, 0, 128], np.uint8)
RED_MAX = np.array([250, 250, 255], np.uint8)


def get_image_from_binary(image_binary):
    np_arr = np.fromstring(image_binary, np.uint8)
    img_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
    return img_np


def get_red_fraction(image):
    red_count = cv2.countNonZero(cv2.inRange(image, RED_MIN, RED_MAX))
    return np.divide(float(red_count), image.size)


if __name__ == '__main__':
    connection = sqlite3.connect('images.db')
    images_df = pd.read_sql('SELECT * FROM IMAGES', connection)

    images_redness = []
    for image_binary in images_df['image']:
        image = get_image_from_binary(image_binary)
        images_redness.append(get_red_fraction(image))

    most_red_image = np.argmax(images_redness)
    image = get_image_from_binary(images_df.loc[most_red_image, 'image'])
    plt.imshow(image)
    plt.show()
