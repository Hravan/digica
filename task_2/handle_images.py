import argparse
import json
import urllib3
from typing import Dict, List, Any

import pandas as pd
import requests
import sqlite3


API_KEY = 'aff624504f08206b39c7948d1dd9f012'

REST_URL = 'https://api.flickr.com/services/rest/'

SEARCH_URL = REST_URL + '?method=flickr.photos.search&api_key={}&format=json&nojsoncallback=1&text={}&' \
                        'page={}'


def get_image(image: Dict[str, Any]):
    """Use flickr image information from given dictionary to retrieve an image."""
    url = 'https://farm{farm}.staticflickr.com/{server}/{id}_{secret}.jpg'
    try:
        res = requests.get(url.format(**image))
    except (urllib3.exceptions.MaxRetryError, requests.exceptions.ConnectionError):
        image = pd.NA
    else:
        image = res.content
    return image


def get_images_df(images_data: List[Dict[str, Any]]) -> pd.DataFrame:
    """Use flickre images information to retrieve images and store them with a data frame with columns id, image."""
    images_df = pd.DataFrame(images_data)[['id']]
    images = [get_image(image_data) for image_data in images_data]
    images_df['image'] = images
    images_df = images_df.dropna(axis=0)
    images_df = images_df.reset_index(drop=True)
    return images_df


def store_images_in_db(images_df):
    """Store given data frame with images in an SQL database."""
    connection = sqlite3.connect('images.db')
    with connection:
        images_df.to_sql('IMAGES', connection, if_exists='replace', index=False)


def get_n_requests(n_images):
    """Determine how many requests returninq 100 images is needed to get all requested images."""
    return n_images // 100 + 1 if n_images > 0 else 0


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--keyword')
    arg_parser.add_argument('--n_images', type=int)
    args = arg_parser.parse_args()

    images_data = None
    if args.keyword is None and args.n_images is None:
        request = REST_URL + f'?method=flickr.photos.getRecent&api_key={API_KEY}&format=json&nojsoncallback=1'
        response = requests.get(request)
        if response.status_code == 200:
            images_data = json.loads(response.text)['photos']['photo']

    if args.keyword is not None and args.n_images is None:
        request = SEARCH_URL.format(API_KEY, args.keyword, 1)
        response = requests.get(request)
        if response.status_code == 200:
            images_data = json.loads(response.text)['photos']['photo']

    if args.keyword is not None and args.n_images is not None:
        n_requests = get_n_requests(args.n_images)
        images_data = []
        for i in range(n_requests):
            request = SEARCH_URL.format(API_KEY, args.keyword, i)
            response = requests.get(request)
            images_data.extend(json.loads(response.text)['photos']['photo'])
        images_data = images_data[:args.n_images]

    if images_data is not None:
        images_df = get_images_df(images_data)
        store_images_in_db(images_df)


